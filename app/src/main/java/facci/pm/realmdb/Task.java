package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Task extends RealmObject {

    @Required
    private String name;
    private  String lastName;
    private String email;
    private  String age;
    @Required
    private String status = TaskStatus.Open.name();

    public Task(String _name , String _lastName, String _email,
                String _age) { this.name = _name;  this.lastName= _lastName; this.email=_email; this.age=_age;}
    public Task() {}


    public void setStatus(TaskStatus status) { this.status = status.name(); }
    public String getStatus() { return this.status; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public  String getlastName() { return lastName;}
    public void setLastName(String lastName) { this.lastName = lastName;}

    public  String getEmail() { return email;}
    public void setEmail(String email) { this.lastName = email;}

    public  String getAge() { return age;}
    public void setAge(String age) { this.lastName = age;}

}
